export class AuthConfig {
  public constructor(
    public readonly loginExpire: string = "7 days",
    public readonly rememberExpire: string = "30 days",
    public readonly activationExpire: string = "24 hours",
    public readonly resetPasswordExpire: string = "24 hours",
    public readonly authKey: string = Math.random().toString(),
    public readonly refreshKey: string = Math.random().toString(),
    public readonly activationKey: string = Math.random().toString(),
    public readonly resetKey: string = Math.random().toString(),
  ) { }
}
