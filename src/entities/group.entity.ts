// Libs
import { BaseEntity, Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";

// Entities
import { ClaimEntity } from "./claim.entity";
import { UserEntity } from "./user.entity";

@Entity()
export class GroupEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({ unique: true })
  public name!: string;

  @ManyToMany(() => UserEntity, (user: UserEntity) => user.groups)
  @JoinTable()
  public users?: UserEntity[];

  @ManyToMany(() => ClaimEntity, (claim: ClaimEntity) => claim.groups)
  @JoinTable()
  public claims?: ClaimEntity[];
}
