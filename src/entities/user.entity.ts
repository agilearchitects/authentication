// Libs
import { BaseEntity, Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";

// Entities
import { ClaimEntity } from "./claim.entity";
import { GroupEntity } from "./group.entity";

@Entity()
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({ unique: true })
  public email!: string;

  @Column()
  public password!: string;

  @Column({ type: Date, nullable: true })
  public activated!: Date | null;

  @Column({ type: Date, nullable: true })
  public banned!: Date | null;

  @ManyToMany(() => GroupEntity, (group: GroupEntity) => group.users)
  public groups?: GroupEntity[];

  @ManyToMany(() => ClaimEntity, (claim: ClaimEntity) => claim.users)
  @JoinTable()
  public claims?: ClaimEntity[];
}
