// Libs
import { BaseEntity, Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";

// Entities
import { GroupEntity } from "./group.entity";
import { UserEntity } from "./user.entity";

@Entity()
export class ClaimEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({ unique: true })
  public name!: string;

  @ManyToMany(() => GroupEntity, (group: GroupEntity) => group.claims)
  public groups?: GroupEntity[];

  @ManyToMany(() => UserEntity, (user: UserEntity) => user.claims)
  public users?: UserEntity[];
}
