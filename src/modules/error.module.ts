export enum ErrorCode {
  NO_USER = 1,
  INTERNAL_ERROR = 2,
}

export class ErrorModule extends Error {
  public constructor(
    public readonly code: ErrorCode,
    message?: string,
  ) {
    super(message);
  }
}
