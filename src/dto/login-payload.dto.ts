import { IUserPayloadDTO, UserPayloadDTO } from "./user-payload.dto";

export interface ILoginPayloadDTO {
  user: IUserPayloadDTO;
  token: string;
  refreshToken?: string;
}

export class LoginPayloadDTO {
  public static parse(loginPayload: ILoginPayloadDTO): LoginPayloadDTO {
    return new this(
      UserPayloadDTO.parse(loginPayload.user),
      loginPayload.token,
      loginPayload.refreshToken,
    );
  }

  public constructor(
    public readonly user: UserPayloadDTO,
    public readonly token: string,
    public readonly refreshToken?: string,
  ) { }

  public serialize(): ILoginPayloadDTO {
    return {
      user: this.user.serialize(),
      token: this.token,
      ...(this.refreshToken !== undefined ? { refreshToken: this.refreshToken } : undefined),
    };
  }
}
