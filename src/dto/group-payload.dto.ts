import { ClaimPayloadDTO, IClaimPayloadDTO } from "./claim-payload.dto";

export interface IGroupPayloadDTO {
  id: number;
  name: string;
  claims?: IClaimPayloadDTO[];
}

export class GroupPayloadDTO {
  public static parse(group: IGroupPayloadDTO):
    GroupPayloadDTO {
    return new this(
      group.id,
      group.name,
      group.claims !== undefined ? group.claims.map((claim: IClaimPayloadDTO) => ClaimPayloadDTO.parse(claim)) : undefined,
    );
  }

  public constructor(
    public readonly id: number,
    public readonly name: string,
    public readonly claims?: ClaimPayloadDTO[],
  ) { }

  public serialize(): IGroupPayloadDTO {
    return {
      id: this.id,
      name: this.name,
      ...(this.claims !== undefined ? { claims: this.claims.map((claim: ClaimPayloadDTO) => claim.serialize()) } : undefined),
    };
  }
}
