export interface IClaimDTO {
  id: number;
  name: string;
}

export class ClaimDTO {
  public static parse(claim: IClaimDTO):
    ClaimDTO {
    return new this(
      claim.id,
      claim.name,
    );
  }

  public constructor(
    public readonly id: number,
    public readonly name: string,
  ) { }

  public serialize(): IClaimDTO {
    return {
      id: this.id,
      name: this.name,
    };
  }
}
