// Libs
import { expect } from "chai";
import { describe, it } from "mocha";

// DTO's
import { ClaimPayloadDTO } from "./claim-payload.dto";
import { UserPayloadDTO } from "./user-payload.dto";

const getUser = (): UserPayloadDTO => {
  return UserPayloadDTO.parse({
    id: 1,
    email: "test@test.test",
    claims: [
      { id: 1, name: "claim1" },
      { id: 2, name: "claim2" },
    ],
    groups: [
      {
        id: 1, name: "group1", claims: [
          { id: 1, name: "claim1" },
          { id: 3, name: "claim3" },
        ]
      },
      {
        id: 2, name: "group2", claims: [
          { id: 4, name: "claim4" },
        ]
      },
    ]
  });
};

describe("UserPayload", () => {
  it("Should be able to verify claim", () => {
    const user = getUser();
    expect(user.hasClaim("claim1")).true;
    expect(user.hasClaim("claim3")).true;
    expect(user.hasClaim("claim4")).true;
    expect(user.hasClaim("claim5")).false;
    expect(user.hasClaim(["claim1", "claim2", "cliam5"], "any")).true;
    expect(user.hasClaim(["cliam5"], "any")).false;
    expect(user.hasClaim(["claim1", "claim2", "cliam5"], "all")).false;
    expect(user.hasClaim(["claim1", "claim2", "claim4"], "all")).true;
  });
  it("Should be able to group all claims", () => {
    const userClaims = getUser().allClaims().map((claim: ClaimPayloadDTO) => ({ id: claim.id, name: claim.name }));
    expect(userClaims).length(4);
    expect(userClaims).eql([
      { id: 1, name: "claim1" },
      { id: 2, name: "claim2" },
      { id: 3, name: "claim3" },
      { id: 4, name: "claim4" },
    ]);
  });
});