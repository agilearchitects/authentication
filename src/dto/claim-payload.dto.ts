import { GroupPayloadDTO, IGroupPayloadDTO } from "./group-payload.dto";
import { IUserPayloadDTO, UserPayloadDTO } from "./user-payload.dto";

export interface IClaimPayloadDTO {
  id: number;
  name: string;
  users?: IUserPayloadDTO[];
  groups?: IGroupPayloadDTO[];
}

export class ClaimPayloadDTO {
  public static parse(claim: IClaimPayloadDTO):
    ClaimPayloadDTO {
    return new this(
      claim.id,
      claim.name,
      claim.users !== undefined ? claim.users.map((user: IUserPayloadDTO) => UserPayloadDTO.parse(user)) : undefined,
      claim.groups !== undefined ? claim.groups.map((group: IGroupPayloadDTO) => GroupPayloadDTO.parse(group)) : undefined,
    );
  }

  public constructor(
    public readonly id: number,
    public readonly name: string,
    public readonly users?: UserPayloadDTO[],
    public readonly groups?: GroupPayloadDTO[],
  ) { }

  public serialize(): IClaimPayloadDTO {
    return {
      id: this.id,
      name: this.name,
      ...(this.users !== undefined ? { users: this.users.map((user: UserPayloadDTO) => user.serialize()) } : undefined),
      ...(this.groups !== undefined ? { groups: this.groups.map((group: GroupPayloadDTO) => group.serialize()) } : undefined),
    };
  }
}
