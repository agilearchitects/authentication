import { ClaimPayloadDTO, IClaimPayloadDTO } from "./claim-payload.dto";
import { GroupPayloadDTO, IGroupPayloadDTO } from "./group-payload.dto";

export interface IUserPayloadDTO {
  id: number;
  email: string;
  claims?: IClaimPayloadDTO[];
  groups?: IGroupPayloadDTO[];
}

export class UserPayloadDTO implements IUserPayloadDTO {
  public static parse(object: IUserPayloadDTO): UserPayloadDTO {
    return new UserPayloadDTO(
      object.id,
      object.email,
      object.claims !== undefined ? object.claims.map((claim: IClaimPayloadDTO) => ClaimPayloadDTO.parse(claim)) : undefined,
      object.groups !== undefined ? object.groups.map((group: IGroupPayloadDTO) => GroupPayloadDTO.parse(group)) : undefined,
    );
  }

  public constructor(
    public readonly id: number,
    public readonly email: string,
    public readonly claims?: ClaimPayloadDTO[],
    public readonly groups?: GroupPayloadDTO[],
  ) { }

  public allClaims(): ClaimPayloadDTO[] {
    return [
      ...(this.claims !== undefined ? this.claims : []),
      ...(this.groups !== undefined ? this.groups.reduce((previousValue: ClaimPayloadDTO[], group: GroupPayloadDTO) => [
        ...previousValue,
        ...(group.claims !== undefined ? group.claims : []),
      ], []) : []),
    ].reduce((previousValue: ClaimPayloadDTO[], claim: ClaimPayloadDTO) => {
      return [
        ...previousValue,
        ...(previousValue.findIndex((findClaim: ClaimPayloadDTO) => findClaim.id === claim.id) === -1 ? [claim] : []),
      ];
    }, []);
  }

  public hasClaim(claim: string | string[], type: "all" | "any" = "any"): boolean {
    if (typeof claim === "string") { claim = [claim]; }
    const userClaims = this.allClaims();
    return claim[type === "all" ? "every" : "some"]((claim: string) => userClaims.findIndex((userClaim: ClaimPayloadDTO) => userClaim.name === claim) !== -1);
  }

  public serialize(): IUserPayloadDTO {
    return {
      id: this.id,
      email: this.email,
      ...(this.claims !== undefined ? { claims: this.claims.map((claim: ClaimPayloadDTO) => claim.serialize()) } : undefined),
      ...(this.groups !== undefined ? { groups: this.groups.map((group: GroupPayloadDTO) => group.serialize()) } : undefined),
    };
  }
}
