export interface ILoginWithoutPasswordDTO {
  email: string;
  remember?: boolean;
}

export class LoginWithoutPasswordDTO implements ILoginWithoutPasswordDTO {
  public static parse(login: ILoginWithoutPasswordDTO): LoginWithoutPasswordDTO {
    return new this(login.email, login.remember);
  }

  public constructor(
    public readonly email: string,
    public readonly remember?: boolean,
  ) { }

  public serialize(): ILoginWithoutPasswordDTO {
    return {
      email: this.email,
      ...(this.remember !== undefined ? { remember: this.remember } : undefined),
    };
  }
}
