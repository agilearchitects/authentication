import { ClaimDTO, IClaimDTO } from "./claim.dto";

export interface IGroupDTO {
  id: number;
  name: string;
  claims?: IClaimDTO[];
}

export class GroupDTO {
  public static parse(group: IGroupDTO):
    GroupDTO {
    return new this(
      group.id,
      group.name,
      group.claims !== undefined ? group.claims.map((claim: IClaimDTO) => ClaimDTO.parse(claim)) : undefined,
    );
  }

  public constructor(
    public readonly id: number,
    public readonly name: string,
    public readonly claims?: ClaimDTO[],
  ) { }

  public serialize(): IGroupDTO {
    return {
      id: this.id,
      name: this.name,
      ...(this.claims !== undefined ? { claims: this.claims.map((claim: ClaimDTO) => claim.serialize()) } : undefined),
    };
  }
}
