// Libs
import { expect } from "chai";
import { describe, it } from "mocha";
import * as typeorm from "typeorm";

// Entities
import { ClaimEntity } from "../entities/claim.entity";
import { GroupEntity } from "../entities/group.entity";
import { UserEntity } from "../entities/user.entity";

// Services
import { ClaimService } from "./claim.service";
import { GroupService } from "./group.service";
import { UserService } from "./user.service";

interface IGroup {
  name: string;
  claims: string[];
}
interface IUser {
  email: string;
  password: string;
  active?: boolean;
  banned?: boolean;
  claims: string[];
  groups: IGroup[];
}

const connection = async (users: IUser[] = [], callback: (userService: UserService) => Promise<void>): Promise<void> => {
  const connection = await typeorm.createConnection({
    type: "sqlite",
    database: ":memory:",
    entities: [UserEntity, GroupEntity, ClaimEntity],
    synchronize: true,
    logging: false,
    dropSchema: true
  });

  try {
    const claimService = new ClaimService(connection.getRepository(ClaimEntity));
    const groupService = new GroupService(connection.getRepository(GroupEntity), claimService);
    const userService = new UserService(
      connection.getRepository(UserEntity),
      claimService,
      groupService,
      typeorm,
    );

    for (const user of users) {
      await userService.create(
        user.email,
        user.password,
        user.active !== undefined ? user.active : true,
        user.banned !== undefined ? user.banned : false,
        user.claims,
        user.groups
      );
    }

    await callback(userService);
  } finally {
    await connection.close();
  }
};

describe("UserService", () => {
  it("Should be able to get all users", async () => {
    await connection([
      { email: "test@test.test", password: "password", active: true, claims: ["test"], groups: [{ name: "admin", claims: ["admin", "new-user"] }] },
      { email: "foo@test.test", password: "password1", banned: true, claims: [], groups: [] },
      { email: "bart@test.test", password: "password2", active: false, claims: [], groups: [] },
    ], async (userService: UserService) => {
      let users = await userService.all();
      expect(users).length(3);
      expect(users[0]).has.property("claims");
      expect((users[0].claims || [])[0].name).equal("test");
      expect(users[0]).has.property("groups");
      expect((users[0].groups || [])[0].claims).length(2);
      users = await userService.all({ isActive: true });
      expect(users).length(1);
      users = await userService.all({ isBanned: true });
      expect(users).length(1);
      users = await userService.all({ isActive: false });
      expect(users).length(1);
    });
  });
});