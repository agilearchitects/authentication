// Libs
import { expect } from "chai";
import { createConnection } from "typeorm";

// Entities
import { BannedTokenEntity } from "../entities/banned-token.entity";

// Services
import { BannedTokenService } from "./banned-token.service";

const connection = async (callback: (bannedTokenService: BannedTokenService<BannedTokenEntity>) => Promise<void>): Promise<void> => {
  const connection = await createConnection({
    type: "sqlite",
    database: ":memory:",
    entities: [BannedTokenEntity],
    synchronize: true,
    logging: false,
    dropSchema: true
  });
  try {
    await callback(new BannedTokenService(connection.getRepository(BannedTokenEntity)));
  } finally {
    await connection.close();
  }
};

describe("BannedTokenService", () => {
  it("Should be able to add banned token", async () => {
    await connection(async (bannedTokenService) => {
      const token = "ABC";
      const bannedToken = await bannedTokenService.ban(token);
      expect(bannedToken).is.not.undefined;
    });
  });
  it("Should be able to get banned token", async () => {
    await connection(async (bannedTokenService) => {
      const token = "ABC";

      await bannedTokenService.ban(token);

      const getBannedToken = await bannedTokenService.get(token);
      expect(getBannedToken).is.not.undefined;
    });
  });
});