// Libs
import { HashtiService } from "@agilearchitects/hashti";
import { JWTService } from "@agilearchitects/jwt";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { describe, it } from "mocha";
import { createConnection } from "typeorm";
import * as typeorm from "typeorm";

// Configs
import { AuthConfig } from "../configs/auth.config";

// Entities
import { BannedTokenEntity } from "../entities/banned-token.entity";
import { ClaimEntity } from "../entities/claim.entity";
import { GroupEntity } from "../entities/group.entity";
import { UserEntity } from "../entities/user.entity";

// Services
import { AuthService } from "./auth.service";
import { BannedTokenService } from "./banned-token.service";
import { ClaimService } from "./claim.service";
import { GroupService } from "./group.service";
import { UserService } from "./user.service";

chai.use(chaiAsPromised);

const connection = async (callback: (authService: AuthService, userService: UserService, claimService: ClaimService, bannedTokenService: BannedTokenService<BannedTokenEntity>, hashtiService: HashtiService) => Promise<void>): Promise<void> => {
  const connection = await createConnection({
    name: Math.random().toString(),
    type: "sqlite",
    database: ":memory:",
    entities: [BannedTokenEntity, UserEntity, ClaimEntity, GroupEntity],
    synchronize: true,
  });
  try {
    const claimService = new ClaimService(connection.getRepository(ClaimEntity));
    const groupService = new GroupService(connection.getRepository(GroupEntity), claimService);
    const bannedTokenService = new BannedTokenService(connection.getRepository(BannedTokenEntity));
    const userService = new UserService(connection.getRepository(UserEntity), claimService, groupService, typeorm);
    const authService = new AuthService(
      new AuthConfig(),
      new BannedTokenService(connection.getRepository(BannedTokenEntity)),
      userService,
      new JWTService(),
      new HashtiService(),
    );
    const hashtiService = new HashtiService();
    await callback(authService, userService, claimService, bannedTokenService, hashtiService);
  } finally {
    await connection.close();
  }
};

describe("AuthService", () => {
  it("Should be able to login, loginwithout password and authenticate and get user's claims, groups and group claims", async () => {
    await connection(async (authService, userService, claimService, bannedTokenService, hashtiService) => {
      const email = "test@test.test";
      const password = "password";

      // Create active user with claims and group
      await userService.create(email, hashtiService.create(password), true, false, ["admin", "moderator"], [{ name: "admin", claims: ["admin", "super"] }]);

      const loginPayload = await authService.login({ email, password });
      const authPayload = await authService.auth(loginPayload.token);
      const loginWithoutPasswordPayload = await authService.loginWithoutPassword({ email });

      for (const payload of [loginPayload.user, authPayload, loginWithoutPasswordPayload.user]) {
        // Check
        expect(payload).has.property("claims");
        expect(payload).has.property("groups");
        expect(payload.claims).length(2);
        expect(payload.groups).length(1);
        expect((payload.groups || [])[0].claims).length(2);
        expect(payload.allClaims()).length(3);
        expect(payload.hasClaim("admin")).to.be.true;
        expect(payload.hasClaim("admin2")).to.be.false;
      }

    });
  });
  it("Should be able to authenticate using a token", async () => {
    await connection(async (authService, userService, claimService, bannedTokenService, hashtiService) => {
      const email = "test@test.test";
      const password = "password";

      // Create active user
      await userService.create(email, hashtiService.create(password), true, false);

      // Login to get token
      const payload = await authService.login({ email, password });

      // Get user using token
      await authService.auth(payload.token);
    });
  });

  it("Should be able to login", async () => {
    await connection(async (authService, userService, claimService, bannedTokenService, hashtiService) => {
      const email = "test@test.test";
      const password = "password";

      // Create active user
      await userService.create(email, hashtiService.create(password), true, false);

      // Login to get token
      const login = await authService.login({ email, password });

      // Check
      expect(login).has.property("user");
      expect(login).has.property("token");
      expect(login.refreshToken).undefined;
    });
  });

  it("Should be able to fail login if user is banned", async () => {
    await connection(async (authService, userService, claimService, bannedTokenService, hashtiService) => {
      const email = "test@test.test";
      const password = "password";

      // Create banned user
      await userService.create(email, hashtiService.create(password), true, true);

      // Try to login
      expect(authService.login({ email, password })).to.be.rejected;
    });
  });

  it("Should be able to fail login if user is not activated", async () => {
    await connection(async (authService, userService, claimService, bannedTokenService, hashtiService) => {
      const email = "test@test.test";
      const password = "password";

      // Create inactive user
      await userService.create(email, hashtiService.create(password), false, false);

      // Try to login
      expect(authService.login({ email, password })).to.be.rejected;
    });
  });

  it("Should be able to login banned user if specified to ignore it", async () => {
    await connection(async (authService, userService, claimService, bannedTokenService, hashtiService) => {
      const email = "test@test.test";
      const password = "password";

      // Create banned user
      await userService.create(email, hashtiService.create(password), true, true);

      // Try to login
      await authService.login({ email, password }, { isBanned: undefined });
    });
  });

  it("Should be able to login inactive user if specified to ignore it", async () => {
    await connection(async (authService, userService, claimService, bannedTokenService, hashtiService) => {
      const email = "test@test.test";
      const password = "password";

      // Create inactive user
      await userService.create(email, hashtiService.create(password), false);

      // Try to login
      await authService.login({ email, password }, { isActive: undefined });
    });
  });

  it("Should be able to login without password", async () => {
    await connection(async (authService, userService, claimService, bannedTokenService, hashtiService) => {
      const email = "test@test.test";
      const password = "password";

      // Create active user
      await userService.create(email, hashtiService.create(password), true);

      // Try to login
      await authService.loginWithoutPassword({ email });
    });
  });

  it("Should be able to login and get refresh token", async () => {
    await connection(async (authService, userService, claimService, bannedTokenService, hashtiService) => {
      const email = "test@test.test";
      const password = "password";

      // Create active user
      await userService.create(email, hashtiService.create(password), true, false);

      // Login to get token
      const login = (await authService.login({ email, password, remember: true }));

      // Check
      expect(login).has.property("refreshToken");
    });
  });
  it("Should be able get a new auth token using a refresh token", async () => {
    await connection(async (authService, userService, claimService, bannedTokenService, hashtiService) => {
      const email = "test@test.test";
      const password = "password";

      // Create active user
      await userService.create(email, hashtiService.create(password), true, false);

      // Login to get token
      const login = (await authService.login({ email, password, remember: true }));

      // Get new login using refresh token
      const newLogin = await authService.refreshToken(login.refreshToken || "");

      // Check
      expect(newLogin).has.property("token");
    });
  });
  it("Should be able to register new user", async () => {
    await connection(async (authService, userService) => {
      const email = "test@test.test";
      const password = "password";

      // Register user
      await authService.register(email, password);

      // Get user
      const user = await userService.get(email);

      // User should not be activated
      expect(user.activated).is.null;

      // Expect user password to be hashed
      expect(user.password).not.equal(password);
    });
  });
  it("Should be able to activate account", async () => {
    await connection(async (authService, userService) => {
      const email = "test@test.test";
      const password = "password";

      // Register user
      const acitvationToken = await authService.register(email, password);

      await authService.activateAccount(acitvationToken);

      // Get user
      const user = await userService.get(email);

      // User should not be activated
      expect(user.activated).is.not.undefined;
    });
  });
  it("Should be able to request password reset", async () => {
    await connection(async (authService) => {
      const email = "test@test.test";
      const password = "password";

      // Register user
      await authService.register(email, password);

      await authService.requestResetPassword(email);

      return;
    });
  });
  it("Should not be able to request password reset for banned user", async () => {
    await connection(async (authService, userService) => {
      const email = "test@test.test";
      const password = "password";

      // Register user
      await authService.register(email, password);

      // Ban user
      await userService.banUser(email);

      // Expect password reset request to fail (since user is banned)
      await expect(authService.requestResetPassword(email)).to.be.rejected;
    });
  });
  it("Should be able to reset password", async () => {
    await connection(async (authService) => {
      const email = "test@test.test";
      const password = "password";
      const newPassword = "password2";

      // Register user
      await authService.register(email, password);

      // Request password reset
      const token = await authService.requestResetPassword(email);
      await authService.resetPassword(token, newPassword);

      const user = await authService.login({ email, password: newPassword });
      expect(user).is.not.undefined;
    });
  });
  it("Should be able to verify token", async () => {
    await connection(async (authService) => {
      const email = "test@test.test";
      const password = "password";

      // Register
      const token = await authService.register(email, password);
      const verified = await authService.verifyToken(token);
      expect(verified).is.true;
    });
  });
  it("Should be able to fail verifying token", async () => {
    await connection(async (authService) => {
      const verified = await authService.verifyToken("");
      expect(verified).is.false;
    });
  });
  it("Should be able to ban token", async () => {
    await connection(async (authService, userService, claimService, bannedTokenService) => {
      const email = "test@test.test";
      const password = "password";

      // Register
      const token = await authService.register(email, password);

      // Ban token
      await bannedTokenService.ban(token);

      // Expect to fail since token is banned
      const verified = await authService.isTokenBanned(token);
      expect(verified).is.true;
    });
  });
});