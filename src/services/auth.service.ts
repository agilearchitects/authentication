// Config
import { AuthConfig } from "../configs/auth.config";

// DTO's
import { ClaimDTO, IClaimDTO } from "../dto/claim.dto";
import { GroupDTO, IGroupDTO } from "../dto/group.dto";
import { LoginPayloadDTO } from "../dto/login-payload.dto";
import { ILoginWithoutPasswordDTO } from "../dto/login-without-password.dto";
import { ILoginDTO } from "../dto/login.dto";
import { IUserPayloadDTO, UserPayloadDTO } from "../dto/user-payload.dto";
import { UserEntity } from "../entities/user.entity";

// Modules
import { ErrorCode, ErrorModule } from "../modules/error.module";

// Services
import { BannedTokenService } from "./banned-token.service";
import { IGetOptions } from "./user.service";

export interface IHashtiService {
    create(plainText: string): string;
    check(plainText: string, hashString: string): boolean;
}
export interface IJWTService {
    sign<T>(payload: T, key: string, expiresIn?: string): string;
    decode<T>(token: string, key: string): Promise<ITokenData<T>>;
}

export interface ITokenData<T> {
    payload: T;
}

export interface IUserService {
  get: (user: number | string, options?: IGetOptions) => Promise<UserEntity>;
  create: (email: string, password: string, active: boolean, banned: boolean) => Promise<UserEntity>;
  activateUser: (id: number) => Promise<UserEntity>;
  resetPassword: (id: number, password: string) => Promise<UserEntity>;
}

export interface IAuthTokenPayload { authUserWithId: number; }
export interface IRefreshTokenPayload { refreshUserWithId: number; }
export interface IActivationTokenPayload { activateUserWithId: number; }
export interface IResetPasswordTokenPayload { resetAccountWithid: number; }
export enum tokenType {
  AUTH = "auth",
  REFRESH = "refresh",
  ACTIVATION = "activation",
  RESET = "reset",
}

export class AuthService {
  public constructor(
    private readonly config: AuthConfig,
    private readonly bannedTokenService: BannedTokenService,
    private readonly userService: IUserService,
    private readonly jwtService: IJWTService,
    private readonly hashtiService: IHashtiService,
  ) { }

  /**
   * Authorize using JWT token
   * @param token Token to authorize with
   */
  public async auth(token: string): Promise<UserPayloadDTO> {
    let tokenData: ITokenData<IAuthTokenPayload> | undefined;
    try {
      // Get token data by decoding it
      tokenData = await this.jwtService.decode<IAuthTokenPayload>(token, this.config.authKey);
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
    // Get user and return
    const user = await this.userService.get(tokenData.payload.authUserWithId);
    return UserPayloadDTO.parse({
      id: user.id,
      email: user.email,
      claims: user.claims !== undefined ? user.claims.map((claim: IClaimDTO) => ClaimDTO.parse(claim).serialize()) : undefined,
      groups: user.groups !== undefined ? user.groups.map((group: IGroupDTO) => GroupDTO.parse(group).serialize()) : undefined,
    });
  }

  /**
   * Login by checking email and verify password agains user model
   * Will return an auth token that can be used with the "auth"-method
   * @param login login details
   * @param options
   */
  public async login(login: ILoginDTO, options: IGetOptions = { isActive: true, isBanned: false }): Promise<LoginPayloadDTO> {
    // Get user
    const user = await this.userService.get(login.email, options);

    // Check provided password with users hashed password
    if (this.hashtiService.check(login.password, user.password)) {
      // Return login payload
      return this.getLoginPayload(user, login.remember !== undefined);
    }
    // Create error
    throw new Error(`Password hash check failed for user ${user.id}`);
  }

  /**
   * Login without using password. Useful for creating authentication token
   * for user even if no password is provide like when user authenticat by
   * other means
   * @param login Login payload
   * @param options Options for
   */
  public async loginWithoutPassword(login: ILoginWithoutPasswordDTO, options: IGetOptions = { isActive: true, isBanned: false }): Promise<LoginPayloadDTO> {
    // Get user
    const user = await this.userService.get(login.email, options);
    // Return login payload
    return this.getLoginPayload(user, login.remember !== undefined);
  }

  /**
   * Use refreshtoken to validate and return user data with a new token
   * @param token
   */
  public async refreshToken(token: string): Promise<LoginPayloadDTO> {
    // Get Token data
    const tokenData = await this.jwtService.decode<IRefreshTokenPayload>(token, this.config.refreshKey);
    // Get user using token data
    const user = await this.userService.get(tokenData.payload.refreshUserWithId);
    // Return user with new token and refresh token
    return this.getLoginPayload(user, true);
  }

  public async register(email: string, password: string): Promise<string> {
    // Create user
    const user = await this.userService.create(
      email,
      this.hashtiService.create(password),
      false, // Not activated
      false, // Not banned
    );

    // Create activation token vaild for 24h
    const activationToken = this.jwtService.sign<IActivationTokenPayload>(
      { activateUserWithId: user.id },
      this.config.activationKey,
      this.config.activationExpire,
    );

    return activationToken;
  }

  public async activateAccount(token: string): Promise<void> {
    // Get data from provided token
    const tokenData = await this.jwtService.decode<IActivationTokenPayload>(token, this.config.activationKey);
    await this.userService.activateUser(tokenData.payload.activateUserWithId);
    return;
  }

  public async requestResetPassword(email: string): Promise<string> {
    // Get user (active or not) by provided email (not banned)
    const user = await this.userService.get(email, { isBanned: false });

    // Create token
    const resetToken = await this.jwtService.sign<IResetPasswordTokenPayload>(
      { resetAccountWithid: user.id },
      this.config.resetKey,
      this.config.resetPasswordExpire,
    );

    return resetToken;
  }

  public async resetPassword(token: string, password: string): Promise<void> {
    // Get token data
    const tokenData = await this.jwtService.decode<IResetPasswordTokenPayload>(token, this.config.resetKey);

    // Get user active or not active (doesn't matter)
    const user = await this.userService.get(tokenData.payload.resetAccountWithid, { isBanned: false });

    // Update password
    await this.userService.resetPassword(user.id, this.hashtiService.create(password));

    // Will activate user if not activated
    if (user.activated === null) {
      await this.userService.activateUser(user.id);
    }
  }

  public async verifyToken(token: string, type?: tokenType): Promise<boolean> {
    let key: string | undefined;
    switch (type) {
    case tokenType.AUTH:
      key = this.config.authKey;
      break;
    case tokenType.REFRESH:
      key = this.config.refreshKey;
      break;
    case tokenType.ACTIVATION:
      key = this.config.activationKey;
      break;
    case tokenType.RESET:
      key = this.config.resetKey;
      break;
    }

    if (key === undefined) {
      for (const type of Object.keys(tokenType)) {
        if (await this.verifyToken(token, (tokenType as never)[type] as tokenType)) {
          return true;
        }
      }
      return false;
    } else {
      try {
        return !!(await this.jwtService.decode(token, key));
      } catch {
        return false;
      }
    }
  }

  public async isTokenBanned(token: string): Promise<boolean> {
    return await this.bannedTokenService.get(token) !== undefined;
  }

  private getLoginPayload(user: IUserPayloadDTO, remember: boolean = false): LoginPayloadDTO {
    try {
      return LoginPayloadDTO.parse({
        user: UserPayloadDTO.parse({
          id: user.id,
          email: user.email,
          claims: user.claims !== undefined ? user.claims.map((claim: IClaimDTO) => ClaimDTO.parse(claim).serialize()) : undefined,
          groups: user.groups !== undefined ? user.groups.map((group: IGroupDTO) => GroupDTO.parse(group).serialize()) : undefined,
        }).serialize(),
        token: this.jwtService.sign<IAuthTokenPayload>(
          { authUserWithId: user.id },
          this.config.authKey,
          this.config.loginExpire,
        ),
        // If login was set to remember user a refresh token is provided as well
        ...(remember === true ? {
          refreshToken: this.jwtService.sign<IRefreshTokenPayload>(
            { refreshUserWithId: user.id },
            this.config.refreshKey,
            this.config.rememberExpire,
          ),
        } : undefined),
      });
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
  }
}
