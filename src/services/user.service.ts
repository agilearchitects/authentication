// Libs
import { ObjectLiteral, Repository, SelectQueryBuilder, WhereExpression, Brackets, DeepPartial } from "typeorm";

// Entities
import { ClaimEntity } from "../entities/claim.entity";
import { GroupEntity } from "../entities/group.entity";
import { UserEntity } from "../entities/user.entity";

// Modules
import { ErrorCode, ErrorModule } from "../modules/error.module";

// Services
import { ClaimService } from "./claim.service";
import { GroupService } from "./group.service";

interface IGroup {
  name: string;
  claims: string[];
}

export interface IGetOptions {
  isActive?: boolean;
  isBanned?: boolean;
  hasClaims?: string[];
}

interface ITypeOrm {
  Brackets: typeof Brackets;
  IsNull: () => unknown;
  Not: (value: unknown) => unknown;
}
export class UserService<T extends UserEntity = UserEntity> {
  public constructor(
    private readonly userRepository: Repository<T>,
    private readonly claimService: ClaimService,
    private readonly groupService: GroupService,
    private readonly typeormModule: ITypeOrm,
  ) { }

  /**
   * Get all users with attached groups, groups's claims and user claims
   * @param options Options for getting users
   */
  public async all(options?: IGetOptions): Promise<T[]> {
    try {
      return await this.getQuery(options).getMany();
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
  }

  /**
   * Get user by email or ID
   * @param email email for user or user ID
   * @param options Options for getting user
   */
  public async get(email: string | number, options?: IGetOptions): Promise<T> {
    let user: T | undefined;
    try {
      user = await this.getQuery(options, email).getOne();
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
    if (user === undefined) { throw new ErrorModule(ErrorCode.NO_USER, "Unable to find user"); }
    return user;
  }

  /**
   * Create new user
   * @param email User email
   * @param password User password
   * @param active Should user be active
   * @param banned Should user be banned
   * @param claims Claims for user
   * @param groups Groups for user
   */
  public async create(email: string, password: string, active: boolean = false, banned: boolean = false, claims: string[] = [], groups: Array<GroupEntity | IGroup | string> = []): Promise<T> {
    let newUser: T;
    try {
      // Create user
      newUser = this.userRepository.create({
        email,
        password,
        activated: active ? new Date() : undefined,
        banned: banned ? new Date() : undefined,
      } as DeepPartial<T>);
      // Save user
      newUser = await this.userRepository.save(newUser as DeepPartial<T>);
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }

    // Add claims to new user. If claims don't exists it will try to create them
    if (claims.length > 0) { newUser = await this.addClaim(newUser, claims, true); }

    // Add user to groups if groups is not empty
    if (groups.length > 0) {
      /* Add user to groups provided. If group don't exists and group claims are
      provided group will be created with claims*/
      newUser = await this.addToGroup(newUser, await Promise.all(groups.map(async (group: GroupEntity | IGroup | string) => {
        let newGroup: GroupEntity;
        // Group is either a string or IGroup type
        if (typeof group === "string" || !("id" in group)) {
          // Get group with name (or create if not exists)
          newGroup = await this.groupService.get(typeof group === "string" ? group : group.name, true);
          // If group is IGroup and group claims is not empty
          if (typeof group !== "string" && group.claims.length > 0) {
            // Add claims to group
            await this.groupService.addClaim(newGroup, group.claims);
          }
        } else {
          // IGroupEntity was provided. Save it to newGroup var
          newGroup = group;
        }
        // Return new group
        return newGroup;
      })));
    }
    // Return created user
    return newUser as T;
  }

  /**
   * Activate user
   * @param user Specify user either by entity, email or ID
   */
  public async activateUser(user: T | string | number): Promise<T> {
    // Get none active user (not banned)
    user = (typeof user === "string" || typeof user === "number") ? await this.get(user, { isActive: false, isBanned: false }) : user;

    // Updated to set user active
    user.activated = new Date();
    try {
      return this.userRepository.save(user as DeepPartial<T>);
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
  }

  /**
   * Decativate user
   * @param user Specify user either by entity, email or ID
   */
  public async deActivateUser(user: T | string | number): Promise<T> {
    // Get active user (not banned)
    user = (typeof user === "string" || typeof user === "number") ? await this.get(user, { isActive: true, isBanned: false }) : user;

    // Updated to set user active
    user.activated = null;
    try {
      return this.userRepository.save(user as DeepPartial<T>);
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
  }

  /**
   * Ban user
   * @param user Specify user either by entity, email or ID
   */
  public async banUser(user: T | string | number): Promise<T> {
    // Get none banned user
    user = (typeof user === "string" || typeof user === "number") ? await this.get(user, { isBanned: false }) : user;

    // Updated to set user active
    user.banned = new Date();
    try {
      return this.userRepository.save(user as DeepPartial<T>);
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
  }

  /**
   * Unban user
   * @param user Specify user either by entity, email or ID
   */
  public async unbanUser(user: T | string | number): Promise<T> {
    // Get banned user
    user = (typeof user === "string" || typeof user === "number") ? await this.get(user, { isBanned: true }) : user;

    // Updated to set user active
    user.banned = null;
    try {
      return this.userRepository.save(user as DeepPartial<T>);
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
  }

  /**
   * Check if provided password matches user password
   * @param user Specify user either by entity, email or ID
   * @param password Password to check
   */
  public async checkPassword(user: T | string | number, password: string): Promise<boolean> {
    // Get user
    user = (typeof user === "string" || typeof user === "number") ? await this.get(user, { isBanned: false }) : user;

    return user.password === password;
  }

  /**
   * Reset user's password
   * @param user Specify user either by entity, email or ID
   * @param password New password
   */
  public async resetPassword(user: T | string | number, password: string): Promise<T> {
    // Get user
    user = (typeof user === "string" || typeof user === "number") ? await this.get(user, { isBanned: false }) : user;

    // Update user password
    user.password = password;
    try {
      return this.userRepository.save(user as DeepPartial<T>);
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
  }

  /**
   * Add claims to user
   * @param user User to add claims to. Can either be specified as an entity, email or ID
   * @param claims Claims to add. Each claim can be specified either as an entity, name or ID
   * @param createClaimIfNotExists Specify if claim should be created if not existing
   */
  public async addClaim(user: T | string | number, claims: Array<string | ClaimEntity>, createClaimIfNotExists: boolean = true): Promise<T> {
    // Set user to user entity
    user = (typeof user === "string" || typeof user === "number") ? await this.get(user) : user;
    // Update claims
    user.claims = [
      ...(user.claims !== undefined ? user.claims : []),
      ...(await Promise.all(claims.map(async (claim: string | ClaimEntity) =>
        typeof claim !== "string" ? claim : await this.claimService.get(claim, createClaimIfNotExists),
      ))),
    ];

    try {
      return this.userRepository.save(user as DeepPartial<T>);
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
  }

  /**
   * Remove claim from user
   * @param user User to remove claims from. Can either be specified as an entity, email or ID
   * @param claims Claims to remove. Each claim can be specified either as an entity, name or ID
   */
  public async removeClaim(user: T | string | number, claims: Array<ClaimEntity | string | number>): Promise<T> {
    // Make sure that user has claims loaded
    user = await this.getUserWithRelations(user);

    try {
      // Filter out claims to remove
      user.claims = [
        ...(user.claims !== undefined ? user.claims.filter((userClaim: ClaimEntity) =>
          claims.findIndex((claim: ClaimEntity | string | number) =>
            (typeof claim === "string" && claim === userClaim.name) ||
            (typeof claim === "number" && claim === userClaim.id) ||
            (typeof claim !== "string" && typeof claim !== "number" && claim.name === userClaim.name),
          ) !== -1,
        ) : []),
      ];
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }

    try {
      return this.userRepository.save(user as DeepPartial<T>);
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
  }

  /**
   * Check if user has claim
   * @param user User to check claims for. Can either be specified as an entity, name or ID
   * @param claims Claims to check. Each claim can be specified either as an entity, name or ID
   */
  public async hasClaim(user: T | string | number, claims: Array<ClaimEntity | string | number>): Promise<boolean> {
    // Make sure that user has claims loaded
    user = await this.getUserWithRelations(user);

    try {
      return [...user.claims !== undefined ? user.claims : []].findIndex((userClaim: ClaimEntity) =>
        claims.findIndex((claim: ClaimEntity | string | number) =>
          (typeof claim === "string" && claim === userClaim.name) ||
          (typeof claim === "number" && claim === userClaim.id) ||
          (typeof claim !== "string" && typeof claim !== "number" && claim.name === userClaim.name),
        ) !== -1,
      ) !== -1 || [...user.groups !== undefined ? user.groups : []].findIndex((userGroup: GroupEntity) =>
        [...userGroup.claims !== undefined ? userGroup.claims : []].findIndex((groupClaim: ClaimEntity) =>
          claims.findIndex((claim: ClaimEntity | string | number) =>
            (typeof claim === "string" && claim === groupClaim.name) ||
            (typeof claim === "number" && claim === groupClaim.id) ||
            (typeof claim !== "string" && typeof claim !== "number" && claim.name === groupClaim.name),
          ) !== -1,
        ) !== -1,
      ) !== -1;
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
  }

  /**
   * Add to user group
   * @param user User to add to group. Can either be specified as an entity, email or ID
   * @param groups groups to add user to. Each group can be specified either as an entity, name or ID
   * @param createGroupIfNotExists Specify if group should be created if not existing
   */
  public async addToGroup(user: T | string | number, groups: Array<string | GroupEntity>, createGroupIfNotExists: boolean = true): Promise<T> {
    // Make sure that user has groups loaded
    user = await this.getUserWithRelations(user);

    // Update groups
    user.groups = [
      ...(user.groups !== undefined ? user.groups : []),
      ...(await Promise.all(groups.map(async (group: string | GroupEntity) =>
        typeof group !== "string" ? group : await this.groupService.get(group, createGroupIfNotExists),
      ))),
    ];

    try {
      return this.userRepository.save(user as DeepPartial<T>);
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
  }

  /**
   * Remove user from group
   * @param user User to remove from group. Can either be specified as an entity, email or ID
   * @param groups groups to remove user from. Each group can be specified either as an entity, name or ID
   */
  public async removeFromGroup(user: T | string | number, groups: Array<string | GroupEntity>): Promise<T> {
    // Make sure that user has groups loaded
    user = await this.getUserWithRelations(user);

    // Filter out groups to remove
    user.groups = [
      ...(user.groups !== undefined ? user.groups.filter((userGroup: GroupEntity) =>
        groups.findIndex((group: GroupEntity | string) => (typeof group === "string" && group === userGroup.name) || (typeof group !== "string" && group.name === userGroup.name)) !== -1,
      ) : []),
    ];

    try {
      return this.userRepository.save(user as DeepPartial<T>);
    } catch (error) {
      throw new ErrorModule(ErrorCode.INTERNAL_ERROR, error);
    }
  }

  /**
   * Check if user is member of group
   * @param user User to check groups for. Can either be specified as an entity, name or ID
   * @param groups Groups to check. Each group can be specified either as an entity, name or ID
   */
  public async isMemberOfGroup(user: T | string | number, groups: Array<string | GroupEntity>): Promise<boolean> {
    // Get user and make sure it has loaded groups relation
    user = (typeof user === "string" || typeof user === "number") ? await this.get(user) : user;
    return [...user.groups !== undefined ? user.groups : []].find((userGroup: GroupEntity) =>
      groups.findIndex((group: GroupEntity | string) => (typeof group === "string" && group === userGroup.name) || (typeof group !== "string" && group.name === userGroup.name)) !== -1,
    ) !== undefined;
  }

  /**
   * Query generator for quering user with claims and other options
   * @param options Query options (isActive, isBanned, has claims)
   * @param email Optional email/id of user to add to where query
   */
  private getQuery(options: IGetOptions = {}, email?: string | number): SelectQueryBuilder<T> {
    let where: ObjectLiteral = {};
    if (typeof email === "string") {
      where = { ...where, email };
    } else if (typeof email === "number") {
      where = { ...where, id: email };
    }

    if (options.isActive === true) {
      /* Active user requires activated field not to be null user not to
      be banned*/
      where = {
        ...where,
        activated: this.typeormModule.Not(this.typeormModule.IsNull()),
        banned: this.typeormModule.IsNull(),
      };
    } else if (options.isActive === false) {
      /* An inactive user requires activated field to be null and user
      not to be banned*/
      where = {
        ...where,
        activated: this.typeormModule.IsNull(),
        banned: this.typeormModule.IsNull(),
      };
    }

    if (options.isBanned === true) {
      // Banned user requires banned field not to be null
      where = {
        ...where,
        banned: this.typeormModule.Not(this.typeormModule.IsNull()),
      };
    } else if (options.isBanned === false) {
      // Non banned user requires banned field to be null
      where = {
        ...where,
        banned: this.typeormModule.IsNull(),
      };
    }
    const query = this.userRepository.createQueryBuilder("user")
      .leftJoinAndSelect("user.claims", "userClaims")
      .leftJoinAndSelect("user.groups", "groups")
      .leftJoinAndSelect("groups.claims", "groupClaims")
      .where(where);
    if (options.hasClaims !== undefined) {
      query.andWhere(((claims: string[]) => new this.typeormModule.Brackets((queryBuilder: WhereExpression) => {
        queryBuilder.where(`"userClaims"."name" IN(${claims.map((claim: string) => this.userRepository.createQueryBuilder().escape(claim)).join(", ")})`)
          .orWhere(`"groupClaims"."name" IN(${claims.map((claim: string) => this.userRepository.createQueryBuilder().escape(claim)).join(", ")})`);
      }))(options.hasClaims));
    }
    return query as SelectQueryBuilder<T>;
  }

  private async getUserWithRelations(user: T | string | number): Promise<T> {
    // Set user to user entity
    user = (typeof user === "string" || typeof user === "number") ? await this.get(user) : user;

    // Make sure user claims is loaded
    if (user.claims === undefined || user.groups === undefined) {
      user = await this.get(user.id);
    }

    return user as T;
  }
}
