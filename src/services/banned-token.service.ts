// Libs
import { DeepPartial, Repository } from "typeorm";

// Entities
import { BannedTokenEntity } from "../entities/banned-token.entity";

export class BannedTokenService<T extends BannedTokenEntity = BannedTokenEntity> {
  public constructor(
    private readonly bannedTokenRepository: Repository<T>,
  ) { }

  /**
   * Get token by token string
   * @param token token string
   * @param createIfNotExists Specify if claim should be created if not found
   */
  public async get(token: string): Promise<T> {
    return await this.bannedTokenRepository.findOneOrFail({ where: { token } }) as T;
  }

  /**
   * Add a token to list of banned tokens
   * @param token Token to add as banned
   */
  public async ban(token: string): Promise<T> {
    const newBannedToken: T = this.bannedTokenRepository.create({ token } as unknown as DeepPartial<T>);
    return await this.bannedTokenRepository.save(newBannedToken as unknown as DeepPartial<T>) as T;
  }

}
