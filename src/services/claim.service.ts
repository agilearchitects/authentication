// Libs
import { DeepPartial, Repository } from "typeorm";

// Entity interfaces
import { ClaimEntity } from "../entities/claim.entity";

export class ClaimService<T extends ClaimEntity = ClaimEntity> {
  public constructor(
    private readonly claimRepository: Repository<T>,
  ) { }

  /**
   * Get All claims with attached users and groups
   */
  public async all(): Promise<T[]> {
    return this.claimRepository.find({ relations: ["users", "groups"] });
  }

  /**
   * Get claim by name or ID
   * @param name name of claim or claim ID
   * @param createIfNotExists Specify if claim should be created if not found
   */
  public async get(name: string | number, createIfNotExists: boolean = false): Promise<T> {
    // Get claim from repository
    let claim: T | undefined = await this.claimRepository.findOne({ where: typeof name === "string" ? { name } : { id: name } });

    // If no claim was found
    if (claim === undefined) {
      // If set to create if not exists and claim was provided as name (not ID)
      if (createIfNotExists === true && typeof name === "string") {
        // Get newly created claim
        claim = await this._create(name);
      } else {
        // Not set to create new claim if not existing will throw error
        throw new Error("Unable to find claim");
      }
    }

    // Return claim
    return claim;
  }

  /**
   * Create new claim
   * @param name Name of claim to create
   * @param getIfExists Specify if claim already exist that claim should be returned
   */
  public create(name: string, getIfExists: boolean = true): Promise<T> {
    // If claim exists, should that claim be returned instead?
    if (getIfExists === true) {
      // Will try to get claim and specify to create it if not existing (result is returned)
      return this.get(name, true);
    } else {
      // Tries to create claim (result is returned)
      return this._create(name);
    }
  }

  /**
   * Internal way of creating claim using repository
   * @param name Name of claim to create
   */
  private async _create(name: string): Promise<T> {
    // Create claim using repository, save and return result
    const newClaim: T = this.claimRepository.create({
      name,
    } as unknown as DeepPartial<T>);
    return await this.claimRepository.save(newClaim as DeepPartial<T>);
  }
}
