// Libs
import { DeepPartial, Repository } from "typeorm";

// Entities
import { ClaimEntity } from "../entities/claim.entity";
import { GroupEntity } from "../entities/group.entity";

// Services
import { ClaimService } from "./claim.service";

export class GroupService<T extends GroupEntity = GroupEntity> {
  public constructor(
    private readonly groupRepository: Repository<T>,
    private readonly claimService: ClaimService,
  ) { }

  /**
   * Get all groups with attached users, users's claims and group claims
   */
  public async all(): Promise<T[]> {
    return (await this.groupRepository.find({ relations: ["users", "users.claims", "claims"] })) as T[];
  }

  /**
   * Get group by name or ID
   * @param name name of group or group ID
   * @param createIfNotExists Specify if group should be created if not found
   */
  public async get(name: string | number, createIfNotExists: boolean = false): Promise<T> {
    // Get group from repository
    let group = await this.groupRepository.findOne({ where: typeof name === "string" ? { name } : { id: name } });

    // If no group was found
    if (group === undefined) {
      // If set to create if not exists and group was provided as name (not ID)
      if (createIfNotExists === true && typeof name === "string") {
        // Get newly created group
        group = await this._create(name);
      } else {
        // Not set to create new group if not existing will throw error
        throw new Error("Unable to find group");
      }
    }
    // Return group
    return group as T;
  }

  /**
   * Create new group
   * @param name Name of group to create
   * @param claims Claims to attached to new group
   * @param getIfExists Specify if group already exist that group should be returned
   */
  public async create(name: string, claims: string[] = [], getIfExists: boolean = true): Promise<T> {
    // Container for new group
    let newGroup: T;
    // If group exists, should that group be returned instead?
    if (getIfExists === true) {
      // Will try to get group and specify to create it if not existing
      newGroup = await this.get(name, true);
    } else {
      // Tries to create group
      newGroup = await this._create(name);
    }

    // Add claims to group
    if (claims.length > 0) { newGroup = await this.addClaim(newGroup, claims, true); }

    // Return group
    return newGroup;
  }

  /**
   * Add claims to group
   * @param group Group to add claims to. Can either be specified as an entity, name or ID
   * @param claims Claims to add. Each claim can be specified either as an entity, name or ID
   * @param createClaimIfNotExists Specify if claim should be created if not existing
   */
  public async addClaim(group: T | string | number, claims: Array<ClaimEntity | string | number>, createClaimIfNotExists: boolean = true): Promise<T> {
    // Set group to group entity
    group = (typeof group === "string" || typeof group === "number") ? await this.get(group) : group;
    // Update claims for group
    group.claims = [
      ...(group.claims !== undefined ? group.claims : []),
      ...(await Promise.all(claims.map(async (claim: ClaimEntity | string | number) =>
        // Parse each claim. If string or array it will try to get claim from repository
        typeof claim !== "string" && typeof claim !== "number" ? claim : await this.claimService.get(claim, createClaimIfNotExists),
      ))),
    ];
    // Save and return group
    return this.groupRepository.save(group as DeepPartial<T>);
  }

  /**
   * Remove claim from group
   * @param group Group to remove claims from. Can either be specified as an entity, name or ID
   * @param claims Claims to remove. Each claim can be specified either as an entity, name or ID
   */
  public async removeClaim(group: T | string | number, claims: Array<ClaimEntity | string | number>): Promise<T> {
    // Get Group and make sure it has loaded claims relation
    if (typeof group === "string" || typeof group === "number") {
      group = await this.get(group);
    } else if (group.claims === undefined) {
      group = await this.get(group.id);
    }
    // Filter out claims to remove
    group.claims = [
      ...(group.claims !== undefined ? group.claims.filter((groupClaim: ClaimEntity) =>
        claims.findIndex((claim: ClaimEntity | string | number) =>
          (typeof claim === "string" && claim === groupClaim.name) ||
          (typeof claim === "number" && claim === groupClaim.id) ||
          (typeof claim !== "string" && typeof claim !== "number" && claim.id === groupClaim.id)) !== -1,
      ) : []),
    ];

    // Save group and return
    return this.groupRepository.save(group as DeepPartial<T>);
  }

  /**
   * Check if group has claim
   * @param group Group to check claims for. Can either be specified as an entity, name or ID
   * @param claims Claims to check. Each claim can be specified either as an entity, name or ID
   */
  public async hasClaim(group: T | string | number, claims: Array<ClaimEntity | string | number>): Promise<boolean> {
    // Get Group and make sure it has loaded claims relation
    if (typeof group === "string" || typeof group === "number") {
      group = await this.get(group);
    } else if (group.claims === undefined) {
      group = await this.get(group.id);
    }

    return [...group.claims !== undefined ? group.claims : []].find((groupClaim: ClaimEntity) =>
      claims.findIndex((claim: ClaimEntity | string | number) =>
        (typeof claim === "string" && claim === groupClaim.name) ||
        (typeof claim === "number" && claim === groupClaim.id) ||
        (typeof claim !== "string" && typeof claim !== "number" && claim.id === groupClaim.id)) !== -1,
    ) !== undefined;
  }

  /**
   * Internal way of creating group using repository
   * @param name Name of group to create
   */
  private async _create(name: string): Promise<T> {
    const newGroup: T = this.groupRepository.create({
      name,
    } as unknown as DeepPartial<T>);
    return this.groupRepository.save(newGroup as DeepPartial<T>);
  }
}
